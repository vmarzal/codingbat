package com.codingbat.functional;

import java.util.List;

public class FunctionalModule {

    public List<Integer> doubling(List<Integer> nums) {
        nums.replaceAll(n -> n * 2);
        return nums;
//        return nums.stream().map(n -> n * 2).collect(Collectors.toList());

    }

}

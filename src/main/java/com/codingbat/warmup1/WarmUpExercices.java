package com.codingbat.warmup1;

public class WarmUpExercices {
	
	/**
	 * We have two monkeys, a and b, and the parameters aSmile and bSmile indicate if each is smiling. 
	 * We are in trouble if they are both smiling or if neither of them is smiling. Return true if 
	 * we are in trouble.
	 * 
	 * monkeyTrouble(true, true) → true
	 * monkeyTrouble(false, false) → true
	 * monkeyTrouble(true, false) → false
	 *
	 */
	public static boolean monkeyTrouble(boolean aSmile, boolean bSmile) {
		return (aSmile && bSmile || !aSmile && !bSmile) ? true : false; 
	}
	
	/**
	 * Given two int values, return their sum. Unless the two values are the same, then return double their sum.
	 * 
	 * sumDouble(1, 2) → 3
	 * sumDouble(3, 2) → 5
	 * sumDouble(2, 2) → 8
	 * 	
	 */
	public static int sumDouble(int a, int b) {
		return (a != b) ? a + b : (a + b) * 2;	
	}

}

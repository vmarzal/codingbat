package com.codingbat.warmup1;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;


public class WarmUpExercicesTest {
	
	@Test
	public void testMonkeyTrouble() {
		assertTrue(WarmUpExercices.monkeyTrouble(true, true));
	}
	
	@Test
	public void testMonkeyTrouble2() {
		assertTrue(WarmUpExercices.monkeyTrouble(false, false));
	}
	
	@Test
	public void testMonkeyTrouble3() {
		assertFalse(WarmUpExercices.monkeyTrouble(true, false));
	}
	
	@Test
	public void testSumDouble() {
		assertThat(WarmUpExercices.sumDouble(1, 2), is(3));
	}
	
	@Test
	public void testSumDouble2() {
		assertThat(WarmUpExercices.sumDouble(3, 2), is(5));
	}
	
	@Test
	public void testSumDouble3() {
		assertThat(WarmUpExercices.sumDouble(2, 2), is(8));
	}
}

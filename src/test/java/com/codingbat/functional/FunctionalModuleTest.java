package com.codingbat.functional;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class FunctionalModuleTest {

    private FunctionalModule module = new FunctionalModule();

    @Test
    public void doubling() throws Exception {
        List<Integer> actual = Arrays.asList(1,2,3);
        List<Integer> expected = Arrays.asList(2,4,6);

        assertNotNull("Response is null", module.doubling(new ArrayList<>()));
        assertThat(module.doubling(actual), is(expected));

        List<Integer> actual2 = Arrays.asList(6, 8, 6, 8, -1);
        List<Integer> expected2 = Arrays.asList(12, 16, 12, 16, -2);

        assertThat(module.doubling(actual2), is(expected2));

        List<Integer> actualEmptyList = Collections.emptyList();
        List<Integer> expectedEmptyList = Collections.emptyList();

        assertThat(module.doubling(actualEmptyList), is(expectedEmptyList));
    }

}